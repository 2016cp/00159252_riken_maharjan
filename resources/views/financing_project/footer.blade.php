<footer class="text-center">
    <div class="footer-above">
        <div class="container">
            <div class="row">
                <div class="footer-col col-md-4">
                    <h3>Money Marathon</h3>
                    <p style="font-size: 20px;">Transform With Us.</p>
                </div>
                <div class="footer-col col-md-4">
                    <h3>Around the Web</h3>
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="footer-col col-md-4">
                    <h3>Location</h3>
                    <p style="font-size: 20px;">Mangal Bazar
                        <br>Lalitpur, Nepal</p>
                </div>
            </div> <!-- end row -->
        </div> <!-- end container -->
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Recane &copy; 2016
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="{{url('lib/js/jquery-3.1.0.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{url('lib/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>

<!-- Plugin JavaScript -->
<script src="{{url('lib/js/jquery.easing.min.js')}}"></script>

<!-- Scrolling Navbar JavaScript -->
<script src="{{url('lib/js/financing.js')}}"></script>

<!-- Script to Activate the Carousel -->
<script>
    $('.carousel').carousel({
        interval: 3000 //changes the speed
    })
</script>

</body>
</html>