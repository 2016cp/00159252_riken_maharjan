<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="{{url('lib/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/font-awesome-4.6.3/css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/css/financing.css')}}">

</head>
<body id="page-top">

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-inverse navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <span>Money Marathon</span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="page-scroll" href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="#about" title="About us">About</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services" title="Our services">Services</a>
                </li>
                <li>
                    <a class="page-scroll" href="#reviews" title="Read what our customers say">Reviews</a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact" title="Contact us">Contact</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('')}}" title="Need any assistance?">Help</a>
                </li>
                <li>
                    <a href="{{url('signup/index')}}" title="Create an account and get more">Signup or Login</a>
                </li>
            </ul>
        </div> <!-- navbar-collapse -->
    </div> <!-- end container -->
</nav>

<!-- Half Page Image Background Carousel Header -->
<header id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for Slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill">
                <img src="{{url('lib/images/caterpillar.jpg')}}" height="100%" width="100%">
            </div>
            <div class="carousel-caption">
                <h2 style="font-size:50px; font-family:'Showcard Gothic','Segoe Script','Kaushan Script';">Transform</h2>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('http://placehold.it/1900x1080&text=Slide Two');">
                <img src="{{url('lib/images/butterfly.jpg')}}" height="100%" width="100%">
            </div>
            <div class="carousel-caption">
                <h2 style="font-size:50px; font-family:'Showcard Gothic','Segoe Script','Kaushan Script';">With</h2>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('http://placehold.it/1900x1080&text=Slide Three');">
                <img src="{{url('lib/images/gold_heart.jpg')}}" height="100%" width="100%">
            </div>
            <div class="carousel-caption">
                <h2 style="font-size:60px; font-family:'Showcard Gothic','Segoe Script','Kaushan Script';">Us</h2>
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>

<!-- About Section -->
<section id="about" class="about-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12" style="margin-bottom: 30px;">
                <h1><small>A little</small> about <small>us.</small></h1>
            </div>
        </div> <!-- end row -->

        <div class="row" style="margin-bottom: 50px;">
            <div class="col-lg-4 col-lg-offset-2">
                <p>Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the complete source files including HTML, CSS, and JavaScript as well as optional LESS stylesheets for easy customization.</p>
            </div>
            <div class="col-lg-4">
                <p>Whether you're a student looking to showcase your work, a professional looking to attract clients, or a graphic artist looking to share your projects, this template is the perfect starting point!</p>
            </div>
        </div> <!-- end row -->

        <div class="row">
            <img src="{{url('lib/images/about.png')}}">
        </div> <!-- end row -->

    </div> <!-- end container -->
</section>

<!-- Services Section -->
<section id="services" class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Our Services</h1>
                <h2><small>Some of our services are mentioned below.</small></h2>
                <img src="{{url('lib/images/services.png')}}" style="margin-left: -30px;">
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>

<!-- Reviews Section -->
<section id="reviews" class="reviews-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h2>Reviews. <small>Check out the reviews.</small></h2>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <blockquote>
                            <p>text text text text tex tetx tex tetx tex t textasdfasdf afasdfas safasdfasfasfas</p>
                            <footer>Author</footer>
                        </blockquote>
                    </div>
                    <div class="col-sm-4">
                        <blockquote>
                            <p>text text text text tex tetx tex tetx tex t textasdfasdf afasdfas safasdfasfasfas</p>
                            <footer>Author</footer>
                        </blockquote>
                    </div>
                    <div class="col-sm-4">
                        <blockquote>
                            <p>text text text text tex tetx tex tetx tex t textasdfasdf afasdfas safasdfasfasfas</p>
                            <footer>Author</footer>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>

<!-- Contact Section -->
<section id="contact" class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Contact Us</h1>
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>

<!-- Footer -->
@include('financing_project.footer')