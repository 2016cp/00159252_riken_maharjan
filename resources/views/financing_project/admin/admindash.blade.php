<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Dashboard</title>
    <link rel="stylesheet" type="text/css" href="{{url('lib/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/font-awesome-4.6.3/css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/css/financing.css')}}">

</head>
<body id="page-top">

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-inverse navbar-custom navbar-fixed-top" role="navigation" style="background-color: #2e3436">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <span>Money Marathon <small style="font-size: 13px">Admin</small></span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="page-scroll" href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/accounttypes')}}" title="Account types">Account types</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/viewcustomers')}}" title="View customers">Customers</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('balance/viewbalance')}}" title="View or update customer balance">Customer balance</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/viewprofile')}}" title="Manage your profile">My Profile</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('login/logout')}}">Logout</a>
                </li>
            </ul>
        </div> <!-- navbar-collapse -->
    </div> <!-- end container -->
</nav>

<section>
    <div class="container" style="margin-top: 100px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="text-align:center;">Welcome Admin <br>
                    <small>You can manage the website functions from here.</small></h1>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
@include('financing_project.footer')