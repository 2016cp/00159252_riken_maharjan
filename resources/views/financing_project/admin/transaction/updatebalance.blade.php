<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Customer Balance</title>
    <link rel="stylesheet" type="text/css" href="{{url('lib/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/font-awesome-4.6.3/css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/css/financing.css')}}">

</head>
<body id="page-top">

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-inverse navbar-custom navbar-fixed-top" role="navigation" style="background-color: #2e3436">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <span>Money Marathon <small style="font-size: 13px">Admin</small> </span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="page-scroll" href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/index')}}" title="Dashboard">Dashboard</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/accounttypes')}}" title="Account types">Account types</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/viewcustomers')}}" title="Customers">Customers</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services" title="Manage your profile">My Profile</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services">Logout</a>
                </li>
            </ul>
        </div> <!-- navbar-collapse -->
    </div> <!-- end container -->
</nav>

<section style="margin-top: 150px;">
    <div class="container">
        <div class="row text-center">
            <a href="{{url('balance/viewbalance')}}" class="links btn btn-warning" title="Back to account types">Back</a>
        </div>
    </div>
</section>

<br>

<section>
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center;">Add or Deduct Customers' balance</div>

                <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Username</th>
                            <th>Interest Rate in % (Account type)</th>
                            <th>Current Balance</th>
                            <th>Insert balance to add</th>
                            <th>Insert balance to deduct</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{($result->id)}}</td>
                                <td>{{($result->username)}}</td>
                                <td>{{($result->account_type)}}</td>
                                <td>{{($result->balance)}}</td>
                                <td>
                                    <form method="post" action="{{url('balance/addbalance')}}/{{$result->id}}">
                                        <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
                                    <div class="form-group">
                                        <input type="text" name="addbalance" id="addbalance" class="form-control" placeholder="Insert balance">
                                    </div>
                                        <div class="form-group">
                                            <input type="submit" name="add-balance" id="add-balance" class="form-control btn btn-primary" value="Add">
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <form method="post" action="{{url('balance/deductbalance')}}/{{$result->id}}">
                                        <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <input type="text" name="deductbalance" id="deductbalance" class="form-control" placeholder="Insert balance">
                                        </div>

                                        <div class="form-group">
                                            <input type="submit" name="deduct-balance" id="deduct-balance" class="form-control btn btn-danger" value="Deduct">
                                        </div>
                                        </form>
                                </td>
                                <td>

                                        <form method="post" action="{{url('balance/calculateinterest')}}/{{$result->id}}">
                                            <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">

                                            <input type="submit" name="calculate-interest" id="calculate-interest" class="form-control btn btn-warning" value="Calculate Interest">

                                        </form>
                                    </td>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
@include('financing_project.footer')