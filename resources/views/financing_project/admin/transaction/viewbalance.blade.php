<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Customer Balance</title>
    <link rel="stylesheet" type="text/css" href="{{url('lib/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/font-awesome-4.6.3/css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/css/financing.css')}}">

</head>
<body id="page-top">

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-inverse navbar-custom navbar-fixed-top" role="navigation" style="background-color: #2e3436">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <span>Money Marathon <small style="font-size: 13px">Admin</small> </span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="page-scroll" href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/index')}}" title="Dashboard">Dashboard</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/accounttypes')}}" title="Account types">Account types</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/viewcustomers')}}" title="Customers">Customers</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services" title="Manage your profile">My Profile</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services">Logout</a>
                </li>
            </ul>
        </div> <!-- navbar-collapse -->
    </div> <!-- end container -->
</nav>

<section>
    <div class="container" style="margin-top: 100px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="text-align:center;">Manage Customers' Balance</h1>
            </div>
        </div>
    </div>
</section>

<br>

<section>
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center;">Customers' balance</div>

                <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Serial Number</th>
                            <th>Username</th>
                            <th>Interest Rate in % (Account type)</th>
                            <th>Balance</th>
                            <th>Total (with interest)</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $row)
                        <tr>
                            <td>{{($row->id)}}</td>
                            <td>{{($row->username)}}</td>
                            <td>{{($row->account_type)}}</td>
                            <td>{{($row->balance)}}</td>
                            <td>{{($row->total)}}</td>
                            <td>
                                <a href="{{url('balance/updatebalance')}}/{{$row->id}}" class="btn btn-info">Update balance</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
@include('financing_project.footer')