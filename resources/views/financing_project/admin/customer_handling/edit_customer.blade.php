<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Customer Details</title>
    <link rel="stylesheet" type="text/css" href="{{url('lib/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/font-awesome-4.6.3/css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/css/financing.css')}}">

</head>
<body id="page-top">

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-inverse navbar-custom navbar-fixed-top" role="navigation" style="background-color: #2e3436">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <span>Money Marathon <small style="font-size: 13px">Admin</small></span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="page-scroll" href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/index')}}" title="Account types">Dashboard</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/accounttypes')}}" title="Account types">Account types</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('balance/viewbalance')}}" title="View or update customer balance">Customer balance</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services" title="Manage your profile">My Profile</a>
                </li>
            </ul>
        </div> <!-- navbar-collapse -->
    </div> <!-- end container -->
</nav>

<section>
    <div class="container" style="margin-top: 150px;">
        <div class="row text-center">
            <a href="{{url('admin/viewcustomers')}}" class="links btn btn-warning" title="Back to customer details">Back</a>
        </div>
    </div>
</section> <br>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="text-align:center;">Edit Customer Details</h1>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 col-md-offset-2">

                <form role="form" method="post" action="{{url('admin/updatecustomer')}}/{{$result->id}}" enctype="multipart/form-data">

                    <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="firstname">First Name</label>
                            <input type="text" name="first_name" id="fname" class="form-control" value="{{$result->fname}}">
                            <span style="color:#FF0000;"> {{ $errors->first('first_name') }} </span>
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="lastname">Last Name</label>
                            <input type="text" name="last_name" id="lname" class="form-control" value="{{$result->lname}}">
                            <span style="color:#FF0000;"> {{ $errors->first('last_name') }} </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" name="address" id="address" class="form-control" value="{{$result->address}}">
                        <span style="color:#FF0000;"> {{ $errors->first('address') }} </span>
                    </div>

                    <div class="form-group">
                        <label for="Phone">Phone</label>
                        <input type="text" name="phone" id="phone" class="form-control" value="{{$result->phone}}">
                        <span style="color:#FF0000;"> {{ $errors->first('phone') }} </span>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="birthday">Birthday</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="date" name="birthday" id="birhtday" class="form-control" value="{{$result->birthday}}">
                            <span style="color:#FF0000;"> {{ $errors->first('birthday') }} </span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="account_type">Account Type</label>
                        </div>
                        <div class="col-sm-6">
                            <select name="account_type" class="form-control">
                                <option value="{{$result->account_type}}">{{$result->account_type}}</option>
                                @foreach($data as $row)
                                    <option value="{{($row->interest_rate)}}">{{($row->account_type_name)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="document">Upload Document</label>
                            <select name="document" class="form-control">
                                <option value="{{$result->doc_type}}">{{$result->doc_type}}</option>
                                <option value="Citizenship">Citizenship</option>
                                <option value="Passport">Passport</option>
                                <option value="License">License</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <img src="{{url($result->doc_image)}}" height="50px" width="200px"> <br>
                            <input type="file" name="document" id="document">
                            <span style="color:#FF0000;"> {{ $errors->first('document') }} </span>
                        </div>

                        <?php session('docimg')->put('doc_image'); ?>

                    </div>

                    <div class="form-group">
                        <label for="Username">Username</label>
                        <input type="text" name="username" id="username" class="form-control" value="{{$result->username}}">
                        <span style="color:#FF0000;"> {{ $errors->first('username') }} </span>
                    </div>

                    <div class="form-group">
                        <label for="email">Email Address</label>
                        <input type="email" name="email" id="email" class="form-control" value="{{$result->email}}">
                        <span style="color:#FF0000;"> {{ $errors->first('email') }} </span>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control" value="{{$result->password}}">
                            <span style="color:#FF0000;"> {{ $errors->first('password') }} </span>
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="cpassword">Confirm Password</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm New Password">
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="hidden" value="customer" name="usertype" /> <!-- User Type -->
                    </div>

                    <div class="form-group">
                        <input type="submit" name="update" id="update" class="form-control btn btn-success" value="Upate Customer Details">
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
@include('financing_project.footer')