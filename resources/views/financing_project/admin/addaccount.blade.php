<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Account Types</title>
    <link rel="stylesheet" type="text/css" href="{{url('lib/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/font-awesome-4.6.3/css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/css/financing.css')}}">

</head>
<body id="page-top">

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-inverse navbar-custom navbar-fixed-top" role="navigation" style="background-color: #2e3436">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <span>Money Marathon <small style="font-size: 13px">Admin</small> </span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="page-scroll" href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/index')}}" title="Dashboard">Dashboard</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/viewcustomers')}}" title="Customers">Customers</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('balance/viewbalance')}}" title="View or update customer balance">Customer balance</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services" title="Manage your profile">My Profile</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services">Logout</a>
                </li>
            </ul>
        </div> <!-- navbar-collapse -->
    </div> <!-- end container -->
</nav>

<section style="margin-top: 150px;">
    <div class="container">
        <div class="row text-center">
            <a href="{{url('admin/accounttypes')}}" class="links btn btn-warning" title="Back to account types">Back</a>
        </div>
    </div>
</section>

<br>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 col-md-offset-2">
            <div class="text-center"><h3><b>Add account types</b></h3></div>

            <form role="form" method="post" action="{{url('admin/addaccount')}}">

                <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">

                <div class="form-group">
                    <label for="account_type_name">Account type name:</label>
                    <input type="text" name="account_type_name" id="account_type_name" tabindex="1" class="form-control" placeholder="Account type name" autocomplete="off">
                    <span style="color:#FF0000;"> {{ $errors->first('account_type_name') }} </span>
                </div>

                <div class="form-group">
                    <label for="interest_rate">Interest rate:</label>
                    <input type="text" name="interest_rate" id="interest_rate" tabindex="2" class="form-control" placeholder="Interest rate" autocomplete="off">
                    <span style="color:#FF0000;"> {{ $errors->first('interest_rate') }} </span>
                </div>

                <div class="form-group row">
                    <div class="col-md-offset-3 col-md-4">
                        <label for="state">Status:</label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="radio" checked="checked" name="status" id="status" value="enable"><label for="enable">Enable</label> &nbsp;
                            <input type="radio" name="status" id="status" value="disable"><label for="disable">Disable</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" name="add-account" id="add-account" tabindex="4" class="form-control btn btn-info">Add</button>
                </div>

            </form>
            </div>
        </div>
        </div>
    </div>
</section>

<!-- Footer -->
@include('financing_project.footer')