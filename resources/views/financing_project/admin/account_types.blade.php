<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Account Types</title>
    <link rel="stylesheet" type="text/css" href="{{url('lib/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/font-awesome-4.6.3/css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/css/financing.css')}}">

</head>
<body id="page-top">

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-inverse navbar-custom navbar-fixed-top" role="navigation" style="background-color: #2e3436">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <span>Money Marathon <small style="font-size: 13px">Admin</small> </span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="page-scroll" href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/index')}}" title="Dashboard">Dashboard</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('admin/viewcustomers')}}" title="Customers">Customers</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('balance/viewbalance')}}" title="View or update customer balance">Customer balance</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services" title="Manage your profile">My Profile</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services">Logout</a>
                </li>
            </ul>
        </div> <!-- navbar-collapse -->
    </div> <!-- end container -->
</nav>

<section style="margin-top: 150px;">
    <div class="container">
        <div class="row text-center">
            <a href="{{url('admin/addaccount')}}" class="links btn btn-success" title="Click to add account types">Add account types</a>
        </div>
    </div>
</section>

<br>

<section>
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center;">Account types</div>
                <div class="panel-body">
                    <p style="text-align:center;">The account types are listed below:</p>
                </div>

                <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Serial Number</th>
                                <th>Account type</th>
                                <th>Interest Rate (in percentage)</th>
                                <th>Date and Time Added</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $row)
                            <tr>
                                <td>{{$row->id}}</td>
                                <td>{{$row->account_type_name}}</td>
                                <td>{{$row->interest_rate}}</td>
                                <td>{{$row->created_at}}</td>
                                <td>{{$row->status}}</td>
                                <td>
                                    <?php if($row->status=="enable"){ ?>
                                    <a href="{{url('admin/disableaccount')}}/{{$row->id}}" class="btn btn-warning">Disable</a>
                                    <?php } else{ ?>
                                    <a href="{{url('admin/enableaccount')}}/{{$row->id}}" class="btn btn-primary">Enable</a>
                                    <?php }?>
                                        |
                                    <a href="{{url('admin/editaccount')}}/{{$row->id}}" class="btn btn-info">Update</a>
                                        |
                                    <a href="{{url('admin/deleteaccount')}}/{{$row->id}}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
@include('financing_project.footer')