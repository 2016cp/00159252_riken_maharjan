<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Signup or Login</title>
    <link rel="stylesheet" type="text/css" href="{{url('lib/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/font-awesome-4.6.3/css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('lib/css/animate.css')}}">

    <link href="{{url('lib/css/financing.css')}}" rel="stylesheet" type="text/css">

</head>
<body>

<script type = "text/javascript"> //Validation of Login form by Javascript
    function validateLogin() {
        //clear fields
        $('#errlogin_email').html("");
        $('#errlogin_password').html("");

        var login_e = $('#login_email').val();
        var login_p = $('#login_password').val();
        var email_regrex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i;

        if (login_e == '') {
            $('#errlogin_email').html("Please enter your email address.");
            return false;
        }
        if(!email_regrex.test(login_e)){
            $('#errlogin_email').html("Please fill in the correct form of the email.");
            return false;
        }
        if (login_p == '') {
            $('#errlogin_password').html("Please enter your current password.");
            return false;
        }
        return true;
    }
</script>

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-inverse navbar-custom navbar-fixed-top" role="navigation" style="background-color: #2e3436">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="{{url('home/index')}}">
                <span>Money Marathon</span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="page-scroll" href="#page-top" id="a"></a>
                </li>
                <li>
                    <a class="page-scroll" href="#about" title="About us">About</a>
                </li>
                <li>
                    <a class="page-scroll" href="#services" title="Our services">Services</a>
                </li>
                <li>
                    <a class="page-scroll" href="#reviews" title="Read what our customers say">Reviews</a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact" title="Contact us">Contact</a>
                </li>
                <li>
                    <a class="page-scroll" href="{{url('home/help')}}" title="Need any assistance?">Help</a>
                </li>

                <li class="dropdown"> <!-- dropdown login form -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Login and enjoy">Login <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-lr animated fadeInDown" role="menu">
                        <div class="col-lg-12">

                            <div class="text-center"><h3><b>Login</b></h3></div>

                            <form method="post" action="{{url('login/index')}}" role="form" autocomplete="off">

                                <input type="hidden" name="_token" value="{{csrf_token()}}">

                                <div class="form-group">
                                    <label class="sr-only" for="login_email">Email address</label>
                                    <input type="email" name="login_email" id="login_email" tabindex="1" class="form-control" placeholder="Email address" autocomplete="off">
                                    <span style="color:#FF0000;"> {{ $errors->first('login_email') }} </span>
                                    <p><span id="errlogin_email" style="color:#FF0000;"></span></p>
                                </div>

                                <div class="form-group">
                                    <label class="sr-only" for="login_password">Password</label>
                                    <input type="password" name="login_password" id="login_password" tabindex="2" class="form-control" placeholder="Password" autocomplete="off">
                                    <span style="color:#FF0000;"> {{ $errors->first('login_password') }} </span>
                                    <p><span id="errlogin_password" style="color:#FF0000;"></span></p>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <input type="checkbox" tabindex="3" name="remember" id="remember">
                                            <label for="remember"> Remember Me</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                        <button type="submit" name="login-submit" id="login-submit" onClick="return validateLogin();" tabindex="4" class="form-control btn btn-primary">Login</button>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="text-center">
                                                <a href="" tabindex="5" class="forgot-password" style="color:#1b6d85; font-size:13px;">Forgot Password?</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </ul>
                </li> <!-- login form end -->
            </ul>
        </div> <!-- navbar-collapse -->
    </div> <!-- end container -->
</nav>

<section>
    <div class="container" style="margin-top: 100px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="text-align:center;">Create an account and login. <br>
                    <small>Get the key to your problems.</small></h1>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="col-sm-6">
            <img src="{{url('lib/images/key.jpg')}}" style="margin-top:100px">
        </div>

            <div class="col-sm-6"> <!-- signup container -->

                <div class="text-center"><h3><b>Signup</b></h3></div>

                <form role="form" method="post" action="{{url('signup/index')}}" enctype="multipart/form-data">

                    <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <input type="text" name="first_name" id="fname" class="form-control" placeholder="First Name*">
                            <span style="color:#FF0000;"> {{ $errors->first('first_name') }} </span>
                        </div>

                        <div class="form-group col-sm-6">
                            <input type="text" name="last_name" id="lname" class="form-control" placeholder="Last Name*">
                            <span style="color:#FF0000;"> {{ $errors->first('last_name') }} </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="address" id="address" class="form-control" placeholder="Address*">
                        <span style="color:#FF0000;"> {{ $errors->first('address') }} </span>
                    </div>

                    <div class="form-group">
                        <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone Number*">
                        <span style="color:#FF0000;"> {{ $errors->first('phone') }} </span>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="birthday">Birthday*</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="date" name="birthday" id="birhtday" class="form-control">
                            <span style="color:#FF0000;"> {{ $errors->first('birthday') }} </span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="account_type">Account Type</label>
                        </div>
                        <div class="col-sm-6">
                            <select name="account_type" class="form-control">
                                @foreach($data as $row)
                                <option value="{{($row->interest_rate)}}">{{($row->account_type_name)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="document_type">Upload Document</label>
                            <select name="document_type" class="form-control">
                                <option value="Citizenship">Citizenship</option>
                                <option value="Passport">Passport</option>
                                <option value="License">License</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <input type="file" name="document" id="document">
                            <span style="color:#FF0000;"> {{ $errors->first('document') }} </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="username" id="username" class="form-control" placeholder="Username*">
                        <span style="color:#FF0000;"> {{ $errors->first('username') }} </span>
                    </div>

                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control" placeholder="Email Address*">
                        <span style="color:#FF0000;"> {{ $errors->first('email') }} </span>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password*">
                            <span style="color:#FF0000;"> {{ $errors->first('password') }} </span>
                        </div>

                        <div class="form-group col-sm-6">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password*">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="question">What is your favourite show?</label>
                        <input type="text" name="security_question" id="security_question" class="form-control" placeholder="Security Answer (in one word)*">
                        <span style="color:#FF0000;"> {{ $errors->first('security_question') }} </span>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="gender">Gender</label>
                        </div>
                        <div class="col-sm-6">
                        <div class="form-group">
                            <input type="radio" name="gender" id="female" value="Female"><label for="female">Female</label> &nbsp;
                            <input type="radio" name="gender" id="male" value="Male"><label for="male">Male</label> &nbsp;
                            <input type="radio" checked="checked" name="gender" id="other" value="Other"><label for="other">Other</label>
                        </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="hidden" value="customer" name="usertype" /> <!-- User Type -->
                    </div>

                    <div class="form-group">
                        <input type="submit" name="register-submit" id="register-submit" class="form-control btn btn-success" value="Signup Now">
                    </div>

                </form>
            </div> <!-- end signup container -->
        </div>
</section>

<!-- Footer -->
@include('financing_project.footer')