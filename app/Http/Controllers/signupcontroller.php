<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Hash;
use App\signupmodel;
use Validator;

class signupcontroller extends Controller
{
    public function getIndex(){
        return view('financing_project/signuporlogin');
    }

    public function postIndex(Request $request){
        $signup = new signupmodel();

        $validation = Validator::make($request->all(),[
           'first_name'=>'required|alpha',
            'last_name'=>'required|alpha',
            'address'=>'required',
            'birthday'=>'required',
            'username'=>'required',
            'email'=>'required',
            'password'=>'required||min:8'
        ]);

        if($validation->fails()){
            return view('financing_project/signuporlogin')->with('errors',$validation->errors());
        }

        $signup->fname = $request->get('first_name');
        $signup->lname = $request->get('last_name');
        $signup->address = $request->get('address');
        $signup->birthday = $request->get('birthday');
        $signup->account_type = $request->get('account_type');
        $signup->username = $request->get('username');
        $signup->email = $request->get('email');
        $signup->password = Hash::make($request->get('password'));
        $signup->gender = $request->get('gender');

        $result = $signup->save();

        if($result){
//            echo "<script>alert('Signup Successful.');</script>";
            return redirect('sl/signup')->with('message', 'Signup Successful!');
        }
        else{
            echo "<script>alert('Signup Failed.');</script>";
        }
    }
}
