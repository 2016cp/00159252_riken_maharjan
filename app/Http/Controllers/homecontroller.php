<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class homecontroller extends Controller
{
    public function getIndex(){
        return view('financing_project/index');
    }

    public function getHeader(){
        return view('financing_project/header');
    }
}
